﻿using System;
using System.Collections.Generic;


namespace FiniteState.Classes
{
    public class Process
    {
   
        //trans...
        Dictionary<StateTransition, ProcessState> transitions;
        //current state 
        public ProcessState CurrentState { get; private set; }

        public Process()
        {
            CurrentState = ProcessState.S0;
            transitions = new Dictionary<StateTransition, ProcessState>
            {
                { new StateTransition(ProcessState.S0, Command.Off), ProcessState.S0 },
                { new StateTransition(ProcessState.S0, Command.On), ProcessState.S1 },
                { new StateTransition(ProcessState.S1, Command.Off), ProcessState.S2 },
                { new StateTransition(ProcessState.S1, Command.On), ProcessState.S0 },
                { new StateTransition(ProcessState.S2, Command.Off), ProcessState.S1 },
                { new StateTransition(ProcessState.S2, Command.On), ProcessState.S2 }
            };
        }

        //get  next 
        public ProcessState getNext(Command command)
        {
            StateTransition transition = new StateTransition(CurrentState, command);
            ProcessState nextState;

            if (!transitions.TryGetValue(transition, out nextState))
                throw new Exception("Invalid transition: " + CurrentState + " -> " + command);

            return nextState;
        }
        //move next
        public ProcessState moveNext(Command command)
        {
            CurrentState = getNext(command);
            return CurrentState;
        }

    }
}
