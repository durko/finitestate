﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FiniteState.Classes
{
    public enum ProcessState
    {
        S0,
        S1,
        S2
    }
}
