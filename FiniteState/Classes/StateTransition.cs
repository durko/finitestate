﻿
using static FiniteState.Classes.Process;

namespace FiniteState.Classes
{
    class StateTransition
    {
        readonly ProcessState current;
        readonly Command command;

        public StateTransition(ProcessState state, Command command)
        {
            this.current = state;
            this.command = command;
        }

        public override int GetHashCode()
        {
            return 17 + 31 * this.current.GetHashCode() + 31 * this.command.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            StateTransition other = (StateTransition)obj;
            return other != null && this.current == other.current && this.command == other.command;
        }
    }
}
