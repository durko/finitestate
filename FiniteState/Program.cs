﻿using System;
using FiniteState.Classes;

namespace FiniteState
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Please enter input string:");
                var input = Console.ReadLine();
                StateLogic.Run(input);
                Console.WriteLine("\nPress any key to continue ...");
                Console.ReadKey();
            }
        }
    }
}
