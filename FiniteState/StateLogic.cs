﻿using FiniteState.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FiniteState
{
    public static class StateLogic
    {
        public static ProcessState Run(string inputString)
        {

            Process process = new Process();
            Console.WriteLine("Initial State = " + process.CurrentState);

            for (int i = 0; i < inputString.Length; i++)
            {
                Console.Write(" Current state:  " + process.CurrentState);
                string input = inputString[i].ToString();
                Command currentCommand = input == "0" ? Command.Off : Command.On;
                Console.WriteLine(" Input:  " + input + " Result state = " + process.moveNext(currentCommand));
            }

            Console.Write("No more input - ");
            Console.WriteLine("Final state = {0}", process.CurrentState);
            return process.CurrentState;

        }
    }
}
