﻿using System;
using FiniteState;
using FiniteState.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestFiniteState
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestExample1()
        {
            string inputString = "110";
            var expectedState = ProcessState.S0;
            var outputState = StateLogic.Run(inputString);
            Assert.AreEqual(expectedState, outputState);
        }

        [TestMethod]
        public void TestExample2()
        {
            string inputString = "1010";
            var expectedState = ProcessState.S1;
            var outputState = StateLogic.Run(inputString);
            Assert.AreEqual(expectedState, outputState);
        }
    }
}
